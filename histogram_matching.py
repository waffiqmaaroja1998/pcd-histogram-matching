# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 21:02:32 2019

@author: Waffiq Maaroja
"""

from PIL import Image
image0 = Image.open('normal0.jpg')       # Gambar 1
image1 = Image.open('normal1.jpg')      # Gambar 2
image2 = Image.open('normal2.jpg')      # Gambar 3

# MENGAMBIL INFORMASI PIXEL PADA CITRA
pixel_map0 = image0.load()
pixel_map1 = image1.load()
pixel_map2 = image2.load()

# INISIALISASI NILAI HISTOGRAM
histogram0 = [0 for i in range(256)]
histogram1 = [0 for i in range(256)]
histogram2 = [0 for i in range(256)]

# TOTAL PIXEL PER CITRA
total_pixel0 = image0.size[0]*image0.size[1]
total_pixel1 = image1.size[0]*image1.size[1]
total_pixel2 = image2.size[0]*image2.size[1]

# MENGHITUNG FREKUENSI PIXEL DENGAN NILAI 0 SAMPAI 255, DIMASUKKAN DALAM HISTOGRAM
for i in range(image0.size[0]):
    for j in range(image0.size[1]):
        histogram0[pixel_map0[i,j][0]] += 1
       
for i in range(image1.size[0]):
    for j in range(image1.size[1]):
        histogram1[pixel_map1[i,j][0]] += 1
        
for i in range(image2.size[0]):
    for j in range(image2.size[1]):
        histogram2[pixel_map2[i,j][0]] += 1
    
# MENGHITUNG FREKUENSI KUMULATIF DARI NILAI PIXEL
for i in range(1, 256):
    histogram0[i] += histogram0[i-1]
    histogram1[i] += histogram1[i-1]
    histogram2[i] += histogram2[i-1]
    
# MENGHITUNG CDF (NORMALISASI FREK KUMULATIF MENJADI BERNILAI 0 SAMPAI 1)
cdf0 = [(histogram0[i]/total_pixel0) for i in range(256)]
cdf1 = [(histogram1[i]/total_pixel1) for i in range(256)]
cdf2 = [(histogram2[i]/total_pixel2) for i in range(256)]

# METHOD UNTUK MENCARI LETAK INDEKS SUATU NILAI PADA HISTOGRAM
def match_value(array_hist, looking_value):
    indeks = -1
    for i in range(256):
        if looking_value <= array_hist[i]:
            break
        indeks = i
    if indeks == -1:
        return 0
    elif indeks == 255:
        return 255
    elif (looking_value - array_hist[indeks]) < (array_hist[indeks+1] - looking_value):
        return indeks
    else:
        return indeks+1
    
# MEMBUAT MAPPING HISTOGRAM CITRA X KE HISTOGRAM CITRA Y
matched_01 = [match_value(cdf1, cdf0[i]) for i in range(256)]
matched_02 = [match_value(cdf2, cdf0[i]) for i in range(256)]

matched_10 = [match_value(cdf0, cdf1[i]) for i in range(256)]
matched_12 = [match_value(cdf2, cdf1[i]) for i in range(256)]

matched_20 = [match_value(cdf0, cdf2[i]) for i in range(256)]
matched_21 = [match_value(cdf1, cdf2[i]) for i in range(256)]

# Histogram Matching citra 1 ke citra 2 dan citra 3
output01 = Image.new(image0.mode, image0.size)
output02 = Image.new(image0.mode, image0.size)
pixel_output01 = output01.load()
pixel_output02 = output02.load()

for i in range(image0.size[0]):
    for j in range(image0.size[1]):
        temp = matched_01[pixel_map0[i,j][0]]
        pixel_output01[i,j] = (temp, temp, temp)
        
for i in range(image0.size[0]):
    for j in range(image0.size[1]):
        temp = matched_02[pixel_map0[i,j][0]]
        pixel_output02[i,j] = (temp, temp, temp)

output01.save("output01.jpg")
output01.close()
output02.save("output02.jpg")
output02.close()

# Histogram Matching citra 2 ke citra 1 dan citra 3
output10 = Image.new(image1.mode, image1.size)
output12 = Image.new(image1.mode, image1.size)
pixel_output10 = output10.load()
pixel_output12 = output12.load()

for i in range(image1.size[0]):
    for j in range(image1.size[1]):
        temp = matched_10[pixel_map1[i,j][0]]
        pixel_output10[i,j] = (temp, temp, temp)
        
for i in range(image1.size[0]):
    for j in range(image1.size[1]):
        temp = matched_12[pixel_map1[i,j][0]]
        pixel_output12[i,j] = (temp, temp, temp)

output10.save("output10.jpg")
output10.close()
output12.save("output12.jpg")
output12.close()

# Histogram Matching citra 3 ke citra 1 dan citra 2
output20 = Image.new(image2.mode, image2.size)
output21 = Image.new(image2.mode, image2.size)
pixel_output20 = output20.load()
pixel_output21 = output21.load()

for i in range(image2.size[0]):
    for j in range(image2.size[1]):
        temp = matched_20[pixel_map2[i,j][0]]
        pixel_output20[i,j] = (temp, temp, temp)
        
for i in range(image2.size[0]):
    for j in range(image2.size[1]):
        temp = matched_21[pixel_map2[i,j][0]]
        pixel_output21[i,j] = (temp, temp, temp)

output20.save("output20.jpg")
output20.close()
output21.save("output21.jpg")
output21.close()

# CLOSE IMAGE
image0.close()
image1.close()
image2.close()